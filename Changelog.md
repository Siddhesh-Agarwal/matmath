# Changelog

_______________________________

## 16 June 2021 - v1.0.0

- Initial Release.
  
## 18 June 2021 - v1.0.1

- Renamed `mmath.py` to `matmath.py`.
- Corrected spelling error.
- Fixed `showwarning()`.

## 24 June 2021 - v1.1.0

- Replaced warnings with errors (ValueError).
- Improved efficiency of `Null()`, `Identity()` and `matSub()`.
- Reduced SLOC.

## 24 June 2021 - v1.1.1

- fixed `order()` function.
